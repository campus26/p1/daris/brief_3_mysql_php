<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="<?php echo ROOTSERVER; ?>/view/accueil.css">
</head>
<body>

<?php
    if (isset($_GET['id'])) 
        $id_ress = $_GET['id'];

    include_once 'php/ressource.model.php';
    $donnees = getRessById($id_ress);
?>

<div class="form_inputs">
    <form action="<?php echo ROOTSERVER; ?>/enregistrer" method="post">
        <div class="form_left">
            <input type="hidden" name="id_r" value="<?= $donnees['id_ressource']; ?>" />
            Nom Bookmark: <input type="text" name="nom_r" value="<?= $donnees['nom_ressource']; ?>" /> <br><br>
            Lien Bookmark: <input type="text" name="lien_r" value="<?= $donnees['lien']; ?>" /> <br><br>
            Date de la ressource: <input type="text" name="date_r" value="<?= $donnees['date_r']; ?>" /><br><br>
            Image Bookmark: <input type="text" name="image_b" />
        </div>
        <div class="form_right"> 
        <br>
        
        Categorie:
        <select name="categorie" value="categorie" class="select">  
        
            <?php  
                getCatForSelect();
            ?>
        
        </select><br>
        Tag:
        <select name="tag" value="tag">  

            <?php  
                getTagForSelect();
            ?>

        </select>
        
            
        
            <p> Description: </p>
            <textarea name="description" rows="5" cols="33"><?php echo $donnees['description']; ?></textarea>
            <input class="submit" type="submit" name="insert" value="Enregistrer" />   
        </div>
    
        <br>
        
    </form>
    <a class="submit" href="<?= ROOTSERVER; ?>/afficher"> Afficher et Modifier les Bookmarks</a>
</div>


</body>
</html>