-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 05 fév. 2021 à 08:50
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bookmark`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_categorie`
--

DROP TABLE IF EXISTS `t_categorie`;
CREATE TABLE IF NOT EXISTS `t_categorie` (
  `id_categorie` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(80) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id_categorie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_categorie`
--

INSERT INTO `t_categorie` (`id_categorie`, `nom`, `description`) VALUES
(1, 'Css', 'Infos Css'),
(2, 'HTML', 'Infos Html'),
(3, 'PHP', 'Learn PHP'),
(4, 'RESSOURCES GENERALES', 'RESSOURCES GENERALES');

-- --------------------------------------------------------

--
-- Structure de la table `t_cat_a_ress`
--

DROP TABLE IF EXISTS `t_cat_a_ress`;
CREATE TABLE IF NOT EXISTS `t_cat_a_ress` (
  `id_categorie` smallint(5) UNSIGNED NOT NULL,
  `id_ressource` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_ressource`,`id_categorie`),
  KEY `t_cat_a_ress_ibfk_1` (`id_categorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_cat_a_ress`
--

INSERT INTO `t_cat_a_ress` (`id_categorie`, `id_ressource`) VALUES
(1, 1),
(1, 4),
(1, 18),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 50),
(1, 51),
(1, 54),
(1, 57),
(1, 58),
(1, 61),
(1, 62),
(1, 63),
(1, 70),
(1, 74),
(1, 76),
(1, 77),
(1, 78),
(1, 79),
(1, 81),
(1, 85),
(1, 86),
(1, 87),
(1, 88),
(1, 89),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 71),
(2, 72),
(2, 73),
(3, 20),
(3, 47),
(4, 2),
(4, 3),
(4, 17),
(4, 39),
(4, 40),
(4, 43),
(4, 44),
(4, 49);

-- --------------------------------------------------------

--
-- Structure de la table `t_ressource`
--

DROP TABLE IF EXISTS `t_ressource`;
CREATE TABLE IF NOT EXISTS `t_ressource` (
  `id_ressource` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom_ressource` varchar(300) DEFAULT NULL,
  `lien` varchar(500) DEFAULT NULL,
  `date_r` date DEFAULT NULL,
  `description` text,
  `Image` varchar(500) DEFAULT NULL,
  `id_user` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_ressource`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_ressource`
--

INSERT INTO `t_ressource` (`id_ressource`, `nom_ressource`, `lien`, `date_r`, `description`, `Image`, `id_user`) VALUES
(1, 'OpenClassRooms2', 'https://openclassrooms.com/fr/courses', '2021-01-23', 'RESSOURCES GENERALES', 'null', 2),
(2, 'test', 'kjhgkijgikug', '2021-01-29', 'RESSOURCES GENERALES', '', 2),
(3, 'W3Schools - HOW TO', 'https://www.w3schools.com/howto/', '2021-01-23', 'RESSOURCES GENERALES', '', 2),
(4, 'A Complete Guide to Flexbox', 'https://css-tricks.com/snippets/css/a-guide-to-flexbox/', '2021-01-23', 'CSS test', '', 2),
(5, 'Le Puy Web - Annuaire Web au Puy-en-Velay', 'https://lepuyweb.fr/home', '2021-01-23', 'RECHERCHE STAGE', '', 2),
(6, 'Exemple', 'http://127.0.0.1/Exo1_Php_Mysql/Exo1.html', '2021-01-28', 'wrgbgewze', 'ffjjffcylms', 2),
(7, 'Exemple', 'http://127.0.0.1/Exo1_Php_Mysql/Exo1.html', '2021-01-28', 'sgddss\r\ngykvg', '', 2),
(8, 'Exemple', 'http://127.0.0.1/Exo1_Php_Mysql/Exo1.html', '2021-01-28', 'fjcjcf', '', 2),
(9, 'OpenClassRooms', 'https://openclassrooms.com/fr/courses', '2021-01-28', 'test', '', 2),
(10, 'OpenClassRooms', 'https://openclassrooms.com/fr/courses', '2021-01-28', 'test 2', '', 2),
(11, 'OpenClassRooms', 'https://openclassrooms.com/fr/courses', '2021-01-28', 'nklkn', '', 2),
(12, 'test2', 'lkjdlj', '2021-01-29', 'test', 'dgeqgre', 2),
(14, 'ess test', 'alknkl', '2021-01-28', 'drgwse\r\nvykyv', 'hgbyigygig', 2),
(17, 'ess test test3', 'alknkl', '2021-01-28', 'Test Test', '', 2),
(18, 'ess test test4', 'alknkl', '2021-01-28', 'test 8', '', 2),
(20, 'ess test test6', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(21, 'ess test test7', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(22, 'ess test test7', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(23, 'ess test test8', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(24, 'ess test test8', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(25, 'ess test test8', 'alknkl', '2021-01-29', NULL, 'hgbyigygig', 2),
(28, 'test 01/02 2', 'vfjjvgyj', '2021-02-01', 'fctntfjc\r\nyccjkjkcykj', 'ffjjffcylms', 2),
(29, 'test 01/02 3', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(30, 'test 01/02 3', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(31, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(32, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(33, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(34, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(35, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(36, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'ydjc', 'ffjjffcylms', 2),
(37, 'test 01/02 4', 'vfjjvgyj', '2021-02-01', 'tyj', 'ffjjffcylms', 2),
(39, 'test 01/02 4', 'www.dell.fr', '2021-02-01', 'k_ykk', 'ffjjffcylms/kljsel.png', 2),
(40, 'test 01/02 5', 'www.dell.fr', '2021-02-01', 'k_ykk', 'ffjjffcylms/kljsel.png', 2),
(43, 'test 01/02 6', 'www.dell.fr', '2021-02-01', 'k_ykk', 'ffjjffcylms/kljsel.png', 2),
(44, 'test 01/02 6', 'www.dell.fr', '2021-02-01', 'k_ykk', 'ffjjffcylms/kljsel.png', 2),
(47, 'jÃ¹jÃ¹', 'www.dell.fr', '2021-02-01', 'kPpp^P', '', 2),
(49, '', '', '2021-02-02', '', '', 2),
(50, 'fgfdg', '', '2021-02-02', '', '', 2),
(51, '', '', '2021-02-02', '', '', 2),
(52, 'fgfdg', 'vfjjvgyj', '2021-02-03', '', 'ffjjffcylms/kljsel.png', 2),
(54, '', '', '2021-02-02', '', '', 2),
(57, '', '', '2021-02-02', '', '', 2),
(58, '', '', '2021-02-02', '', '', 2),
(59, ':nom_r', ':lien_r', '2021-02-02', ':description_r', ':image_b', 3),
(60, ':nom_r', ':lien_r', '2021-02-02', ':description_r', ':image_b', 3),
(61, 'fgfdg', 'gfdgsfdg', '2021-02-02', 'dfgsdfgsdf', 'gdfsgfgd', 2),
(62, 'fgfdg', 'gfdgsfdg', '2021-02-02', 'dfgsdfgsdf', 'gdfsgfgd', 2),
(63, 'fgfdg', 'gfdgsfdg', '2021-02-02', 'dfgsdfgsdf', 'gdfsgfgd', 2),
(70, '', '', '2021-02-03', '', '', 2),
(71, 'test 03/02 4', 'www.dell.fr', '2021-02-03', 'test', 'ffjjffcylms/kljsel.png', 2),
(72, 'test 01/02', '', '2021-02-03', '', '', 2),
(73, 'test 01/02', 'vfjjvgyj', '2021-02-03', '', 'ffjjffcylms', 2),
(74, '', '', '2021-02-03', '', '', 2),
(76, '', '', '2021-02-03', '', '', 2),
(77, '', '', '2021-02-03', '', '', 2),
(78, '', '', '2021-02-03', '', '', 2),
(79, '', '', '2021-02-03', '', '', 2),
(81, '', '', '2021-02-03', '', '', 2),
(85, '', '', '2021-02-03', '', '', 2),
(86, '', '', '2021-02-03', '', '', 2),
(87, '', '', '2021-02-03', '', '', 2),
(88, '', '', '2021-02-03', '', '', 2),
(89, '', '', '2021-02-05', '', '', 2);

-- --------------------------------------------------------

--
-- Structure de la table `t_ress_a_tag`
--

DROP TABLE IF EXISTS `t_ress_a_tag`;
CREATE TABLE IF NOT EXISTS `t_ress_a_tag` (
  `id_ressource` smallint(5) UNSIGNED NOT NULL,
  `id_tag` smallint(5) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_ressource`,`id_tag`),
  KEY `fk_ress_tag` (`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_ress_a_tag`
--

INSERT INTO `t_ress_a_tag` (`id_ressource`, `id_tag`) VALUES
(57, 1),
(58, 1),
(61, 1),
(62, 1),
(63, 1),
(70, 1),
(71, 1),
(72, 1),
(74, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(81, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(4, 2),
(1, 3),
(73, 3),
(7, 5);

-- --------------------------------------------------------

--
-- Structure de la table `t_tag`
--

DROP TABLE IF EXISTS `t_tag`;
CREATE TABLE IF NOT EXISTS `t_tag` (
  `id_tag` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_tag`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_tag`
--

INSERT INTO `t_tag` (`id_tag`, `nom`) VALUES
(1, 'PHP'),
(2, 'CSS'),
(3, 'HTML'),
(4, 'Jeux'),
(5, 'News');

-- --------------------------------------------------------

--
-- Structure de la table `t_utilisateur`
--

DROP TABLE IF EXISTS `t_utilisateur`;
CREATE TABLE IF NOT EXISTS `t_utilisateur` (
  `id_utilisateur` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) DEFAULT NULL,
  `prenom` varchar(40) DEFAULT NULL,
  `adresse` text,
  PRIMARY KEY (`id_utilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_utilisateur`
--

INSERT INTO `t_utilisateur` (`id_utilisateur`, `nom`, `prenom`, `adresse`) VALUES
(1, 'Baksic', 'Daris', '5 Avenue Foch 43000 Le Puy en Velay'),
(2, 'Dupont', 'Thomas', '21 Avenue des Belges 43000 Le Puy en Velay'),
(3, 'Gilbert', 'Martin', '25 Place du Breuil 43000 Le Puy en Velay'),
(4, 'test', 'test2', 'jksq:kj:kk:'),
(5, 'Bruno', 'Dupont', '2 Palce du Breuil 43000 Le Puy en Velay'),
(6, 'Marie', 'Malherbe', '2 Palce du Breuil 43000 Le Puy en Velay'),
(7, 'Jean', 'Martin', '2 Palce du Breuil 43000 Le Puy en Velay'),
(8, 'Becky', 'Martinez', '2 Palce du Breuil 43000 Le Puy en Velay'),
(9, 'Becky', 'Martinez', '2 Palce du Breuil 43000 Le Puy en Velay'),
(10, 'Becky', 'Martinez', '2 Palce du Breuil 43000 Le Puy en Velay'),
(11, 'Becky', 'Martinez', '2 Palce du Breuil 43000 Le Puy en Velay'),
(12, 'Bernard', 'Martinez', '25 Avenue Foch 43000 Le Puy en Velay'),
(13, 'Bernard', 'Martinez', '25 Avenue Foch 43000 Le Puy en Velay'),
(14, 'Bernard', 'Martinez', '25 Avenue Foch 43000 Le Puy en Velay');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_cat_a_ress`
--
ALTER TABLE `t_cat_a_ress`
  ADD CONSTRAINT `t_cat_a_ress_ibfk_1` FOREIGN KEY (`id_categorie`) REFERENCES `t_categorie` (`id_categorie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `t_cat_a_ress_ibfk_2` FOREIGN KEY (`id_ressource`) REFERENCES `t_ressource` (`id_ressource`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `t_ressource`
--
ALTER TABLE `t_ressource`
  ADD CONSTRAINT `fk_ress_util` FOREIGN KEY (`id_user`) REFERENCES `t_utilisateur` (`id_utilisateur`);

--
-- Contraintes pour la table `t_ress_a_tag`
--
ALTER TABLE `t_ress_a_tag`
  ADD CONSTRAINT `fk_ress_tag` FOREIGN KEY (`id_tag`) REFERENCES `t_tag` (`id_tag`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_ress_tag2` FOREIGN KEY (`id_ressource`) REFERENCES `t_ressource` (`id_ressource`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
