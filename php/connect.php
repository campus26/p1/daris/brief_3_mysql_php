<?php

    function connect(){
        $host = 'localhost';
        $dbname = 'bookmark';
        $username = 'root';
        $password = '';

        try {
        // se connecter à mysql
        $pdo = new PDO("mysql:host=$host;dbname=$dbname","$username","$password");
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $pdo;
        } 
        catch (PDOException $exc) {
        echo $exc->getMessage();
        exit();
        }
    }

?>