<?php



function getAllRessource() {   
    include_once 'connect.php'; 
    $pdo = connect();

    // On recupere tous les bookmarks
    $reponse = $pdo->prepare('SELECT t_ressource.id_ressource, t_ressource.nom_ressource, t_ressource.lien, t_ressource.date_r, t_ressource.description FROM t_ressource');
    
    /*$reponse = $pdo->prepare('SELECT t_ressource.id_ressource, t_ressource.nom_ressource, t_ressource.lien, t_ressource.date_r, t_ressource.description, t_categorie.nom FROM t_ressource 
    LEFT JOIN t_cat_a_ress ON t_ressource.id_ressource = t_cat_a_ress.id_ressource
    LEFT JOIN t_categorie ON t_cat_a_ress.id_categorie = t_categorie.id_categorie ORDER BY id_ressource');*/
    

    $reponse->execute();

    return  $reponse->fetchall();
}

function getCatByIdRess($id_ress){
    include_once 'connect.php'; 
    $pdo = connect();
    $reponse = $pdo->prepare('SELECT t_categorie.nom FROM t_ressource 
    JOIN t_cat_a_ress ON t_ressource.id_ressource = t_cat_a_ress.id_ressource 
    JOIN t_categorie ON t_cat_a_ress.id_categorie = t_categorie.id_categorie WHERE t_ressource.id_ressource = ?');
    $reponse->execute(array($id_ress));
    return  $reponse->fetchall();
    
}


function getTagName() {
    $pdo = connect();
    $reponse2 = $pdo->prepare('SELECT t_ressource.id_ressource, t_tag.nom FROM t_ressource 
        LEFT JOIN t_ress_a_tag ON t_ressource.id_ressource = t_ress_a_tag.id_ressource
        LEFT JOIN t_tag ON t_ress_a_tag.id_tag = t_tag.id_tag ORDER BY id_ressource');
        $reponse2->execute();

        return  $reponse2->fetchall();
}

function displayAllRess(){
//if( isset($_POST['afficher']) ){
    echo "Bonjour<br>";
    $pdo = connect();
    
    $donnees = getAllRessource();

    //$tags = getTagName();

    // On affiche le resultat
    foreach($donnees as $d )
    {
        //var_dump($d);
        //On affiche
        echo "</TR>";
        echo "<TH> $d[id_ressource] </TH>";
        echo "<TH> $d[nom_ressource] </TH>";
        echo "<TH> $d[lien] </TH>";
        echo "<TH> $d[date_r] </TH>";
        echo "<TH> $d[description] </TH>";
        echo "<TH> $d[nom] </TH>";  //nom de cat.
        //echo "<TH> $t[nom] </TH>"; //nom du Tag
        echo "<TH><a href=\"index.php?page=modif&id=$d[id_ressource]\"> Modifier </a></TH>";
        echo "<TH><a href=\"index.php?page=suppr&id=$d[id_ressource]\"> Supprimer </a></TH>";
        
        echo "</TR>";
        
    }

    echo '</TABLE>';
}

function getRessById($id_ress){
    if (isset($id_ress)) 
    {
        include_once 'connect.php';
        $pdo = connect();

        $reponse = $pdo->prepare("SELECT * FROM t_ressource WHERE id_ressource= ?");
        $reponse->execute( array($id_ress) );
        return ( $reponse->fetch(PDO::FETCH_ASSOC) );
        //var_dump($donnees);
    }
    else 
    {
        echo 'Il faut renseigner un ID !';
    }    
}

function getCatForSelect(){
    include_once 'connect.php';
    $pdo = connect();
    var_dump($_GET);

    $sql = 'SELECT nom FROM t_categorie';
        $res = $pdo->prepare($sql);
        if ($res->execute()) {
            while ($row = $res->fetch()) {
                echo '<option>'.$row["nom"].'</option><br/>';
            }
        }
}

function getTagForSelect(){
    include_once 'connect.php';
    $pdo = connect();
    
    $sql = 'SELECT nom FROM t_tag';
    $res = $pdo->prepare($sql);
    
    var_dump($res);
    if ($res->execute()) {
        while ($row = $res->fetch()) {
            echo '<option>'.$row["nom"].'</option><br/>';
        }
    }
}

function countAllRess(){
    include_once 'connect.php';
    $pdo = connect();
    
    $sql = 'SELECT COUNT(*) FROM t_ressource';
    $res = $pdo->prepare($sql);
    $res->execute();
    $t_total = $res -> fetch();
    return $t_total['COUNT(*)'];
}

?>